<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('pages.register');
    }

    public function sent(Request $request)
    {
        // dd($request->all());

        $first_name = $request['FName'];
        $last_name = $request['LName'];

        return view('pages.welcome', ['FName' => $first_name, 'LName' => $last_name]);
    }
}
