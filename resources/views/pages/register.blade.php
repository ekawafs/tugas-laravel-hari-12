<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Registrasi</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="/sent" method="POST">
        @csrf
        <label for="FName">First name:</label><br><br>
        <input type="text" id="FName" name="FName"><br><br>
        <label for="LName">Last name:</label><br><br>
        <input type="text" name="LName" id="LName"><br>

        <p>Gender:</p>
        <input type="radio" name="Gender" id="male" value="Male">
        <label for="male">Male</label><br>
        <input type="radio" name="Gender" id="female" value="Female">
        <label for="female">Female</label><br>
        <input type="radio" name="Gender" id="other" value="Other">
        <label for="other">Other</label>
        <br>

        <p>Nationality:</p>
        <select name="Nation" id="">
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Kamboja">Kamboja</option>
            <option value="Thailand">Thailand</option>
            <option value="Singapure">Singapure</option>
        </select><br>

        <p>Language Spoken:</p>
        <input type="checkbox" name="Spoken" id="indonesia" value="Indonesia">
        <label for="indonesia">Indonesia</label><br>
        <input type="checkbox" name="Spoken" id="english" value="English">
        <label for="english">English</label><br>
        <input type="checkbox" name="Spoken" id="other" value="Other">
        <label for="other">Other</label><br>

        <p>Bio:</p>
        <textarea name="Bio" id="" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>